module.exports = function(grunt) {

	grunt.initConfig({
	  watch: {
	  	js: {
		  files: ['themes/src/js/**/*.js'],
		  tasks: ['uglify'],
	    },
	    css: {
		  files: ['themes/src/css/**/*.css'],
		  tasks: ['cssmin'],
	    },
	    scss: {
		  files: ['themes/src/sass/**/*.scss'],
		  tasks: ['sass', 'cssmin'],
	    },
	    image: {
		  files: ['themes/src/images/**/*.{png,jpg,gif,svg}'],
		  tasks: ['imagemin'],
	    },
	   //  css: {
		  // files: ['src/css/**/*.css'],
		  // tasks: ['concat'],
	   //  },
	  },
      uglify: {
	    options: {
	      mangle: false
	    },
	    my_target: {
	      files: {
	        'themes/build/js/script.min.js': ['themes/src/js/script.js'],
          'themes/build/js/vendor/jquery.min.js': ['themes/src/components/jquery/jquery.js'],
	      }
	    }
	  },
	  sass: {                              					 // Task
	    scss: {                            				     // Target
	      options: {                       					 // Target options
	        style: 'compressed',							 // compressed, expanded
	        sourcemap: 'none',
	      },
	      files: {                         					 // Dictionary of files
	        'themes/src/css/admin-style.css': 'themes/src/sass/admin-style.scss',       // 'destination': 'source'
	      },
	    },
	  },
	  imagemin: {
	  	dynamic: {
	  		files: [{
	  			expand: true,
	  			cwd: 'themes/src/images/',
	  			src: ['**/*.{png,jpg,gif}'],
	  			dest: 'themes/build/images/'
	  		}]
	  	}
      },
      cssmin: {
      	my_target: {
      		files: [{
      			expand: true,
      			cwd: 'themes/src/components/normalize-css/',
      			src: ['*.css'],
      			dest: 'themes/build/css/vendor/',
      			ext: '.css'
      		},
          {
            expand: true,
            cwd: 'themes/src/css/',
            src: ['*.css'],
            dest: 'themes/build/css/',
            ext: '.css'
          }]
      	}
      }
	});

	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.registerTask('default', ['uglify', 'sass', 'imagemin', 'cssmin', 'watch']);
};
